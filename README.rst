About
=====

RDRF ( Rare Disease Registry Framework ) is a tool for 
the creation of web-based data entry forms and questionnaires based
on reusable data element definitions ( "Common Data Elements" ) which
can be created and/or loaded into the system at runtime without changes
to the source code. RDRF has been developed at the `Centre of Comparative
Genomics <http://ccg.murdoch.edu.au>`_

Links
=====

`Demo Site <https://rdrf.ccgapps.com.au/demo/>`_

`Source Code <https://bitbucket.org/ccgmurdoch/rdrf>`_

`Email <mailto:rdrf@ccg.murdoch.edu.au>`_

`Documentation <http://rare-disease-registry-framework.readthedocs.org/en/latest/>`_


Publications
============
Matthew I Bellgard, Lee Render, Maciej Radochonski and Adam Hunter, Second generation registry framework, Source Code Biol Med. 2014 Jun 20;9:14.

Matthew Bellgard, Christophe Beroud, Kay Parkinson, Tess Harris, Segolene Ayme, Gareth Baynam, Tarun Weeramanthri, Hugh Dawkins and Adam Hunter, Dispelling myths about rare disease registry system development. Source Code for Biology and Medicine, 2013. 8(1): p. 21.

Rodrigues M, Hammond-Tooke G, Kidd A, Love D, Patel R, Dawkins H, Bellgard M, Roxburgh R, The New Zealand Neuromuscular Disease Registry. J Clin Neurosci, 2012. 19(12): p. 1749-50.

Bellgard MI, Macgregor A, Janon F, Harvey A, O'leary P, Hunter A and Dawkins H, A modular approach to disease registry design: successful adoption of an internet-based rare disease registry. Hum Mutat 33: E2356-2366.
