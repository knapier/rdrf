.. _registries:

Registries
----------

In RDRF, a "registry" is just a named collection of :ref:`forms`. A Registry is created
in the admin interface by selecting the "> Rdrf" left side bar option and choosing "Registrys" and
clicking "Add registry".

A Registry must have a non-blank name and a code ( which should contain spaces.)
