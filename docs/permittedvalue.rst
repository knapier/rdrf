.. _permittedvalue:
Permitted Values
================

Definition
----------

A permitted value is just a single allowed value that comprises part of a value range.

Examples
--------

If the associated value range was intended to be a size range then examples might be

* small
* medium
* large

In the GUI , individual permitted values will appear as selectable options in a drop down list.

 

