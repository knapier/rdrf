.. rdrf documentation master file, created by
   sphinx-quickstart on Tue Jan 14 10:39:07 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to rdrf's documentation!
================================

Contents:

.. toctree::
   :maxdepth: 3

   workflow
   designmode
   gui
   registries
   howtocreatearegistry
   forms
   sections
   cdes
   permittedvaluegroups
   permittedvalue
   datatypes
   export
   import
   questionnaires
   interfacing
   about
   development


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

