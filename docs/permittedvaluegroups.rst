.. _permittedvaluegroup:
Permitted Value Group
=====================

Definition
----------
A permitted value group (PVG) is a set of :ref:`Permitted Values <permittedvalue>`.
PVGS are defined as in the admin interface by navigating to "Home >> Rdrf >> Cde Permitted Value Groups".

Click "Add cde permitted value group" button to create a new one.

A value group has a code which  must be a unique non blank string value.

Once a permitted value group has been created, add :ref:`permitted values <permittedvalue>` to it.

Examples
--------
The decoupling of permitted value groups from cdes that make use of them allows different attributes 

( e.g. shoe size , hat size ) to share the same value ranges ( E.g. small, medium, large ) as is done
in NINDS http://www.commondataelements.ninds.nih.gov )



