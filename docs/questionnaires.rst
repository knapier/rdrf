.. _questionnaires:

Questionnaires
==============

To fill out a questionnaire for registry with code fh  ( for example)

The member of the public navigates to the URL:    <RDRF URL>/fh/questionnaire
where RDRF URL is the site URL of the RDRF instance. ( E,g, https://ccgapps.com.au/demo-rdrf/fh/questionnaire )

