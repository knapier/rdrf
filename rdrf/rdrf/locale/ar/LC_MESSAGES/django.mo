��          �      �           	       "   0  #   S     w     �  	   �     �     �     �     �     �     �     �                "  
   B     M     V     e     }  �  �     5  /   K  C   {  C   �           $     @  
   O  
   Z     e  (   t     �     �  &   �     �  "      1   #  
   U     `     g  2   �     �                          
                                                    	                          Address type Age at Clinical Diagnosis Age in years at clinical diagnosis Age in years at molecular diagnosis Change password Data Elements Diagnosis Documentation Genetic Groups Import Registry Definition Log out Patients Questionnaire Responses Questionnaires Quick access links Rare Disease Registry Framework Registries Registry Registry Forms Show full list of links Yes Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-02-03 14:20+0800
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;
 نوع العنوان العمر عند التشخيص السريري العمر في السنوات عند التشخيص السريري العمر في السنوات عند التشخيص الجزيئي تغيير كلمة المرور عناصر البيانات التشخيص توثيق وراثي مجموعات تعريف استيراد التسجيل تسجيل الخروج المرضى الردود على الاستبيان الاستبيانات وصلات سريعة الوصول الإطار سجل الأمراض النادرة سجلات سجل نماذج التسجيل تظهر قائمة كاملة من الروابط نعم 